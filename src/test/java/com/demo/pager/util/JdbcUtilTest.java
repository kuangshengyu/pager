package com.demo.pager.util;

import org.junit.Test;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by kuang on 12/12 16:17
 */
public class JdbcUtilTest {

    @Test
    public void testGetConnection() throws Exception {
        JdbcUtil jdbcUtil = new JdbcUtil();
        System.out.print(jdbcUtil.getConnection());

    }

    @Test
    public void testUpdateByPreparedStatement() throws Exception {

    }

    @Test
    public void testFindResult() throws Exception {
        JdbcUtil jdbcUtil = new JdbcUtil();
        jdbcUtil.getConnection();
        try {
            List<Map<String, Object>> result = jdbcUtil.findResult(
                    "select * from t_student", null);
            for (Map<String, Object> m : result) {
                System.out.println(m);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            jdbcUtil.releaseConn();
        }
    }

    @Test
    public void testReleaseConn() throws Exception {

    }
}