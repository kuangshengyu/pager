package com.demo.pager.entity;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kuang on 12/12 23:20
 */
public class PagerTest {

    @Test
    public void testPager(){

        List<Student> students=new ArrayList<>();
        students.add(new Student());
        students.add(new Student());
        students.add(new Student());
        students.add(new Student());
        students.add(new Student());
        students.add(new Student());
        students.add(new Student());
        students.add(new Student());
        Pager pager=new Pager(1,5,students);
        Assert.assertEquals(2,pager.getTotalPage());
    }

}