package com.demo.pager.util;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.*;

/**
 * Created by kuang on 12/12 15:54
 */
public class JdbcUtil {
    //定义数据库的用户名
    private static String USERNAME;
    //定义数据库的密码
    private static String PASSWORD;
    //定义数据库的驱动信息
    private static String DRIVER;
    //定义数据库的访问地址
    private static String URL;

    //定义数据库的链接
    private Connection connection;

    //定义SQL语句的执行对象
    private PreparedStatement pstmt;

    //定义查询返回的结果集合
    private ResultSet resultSet;

    public JdbcUtil() {
    }

    static {
        loadConfig();
    }

    public static void loadConfig() {
        try {
            InputStream inputStream = JdbcUtil.class.getResourceAsStream("/jdbc.properties");
            Properties properties = new Properties();
            properties.load(inputStream);
            USERNAME = properties.getProperty("jdbc.username");
            PASSWORD = properties.getProperty("jdbc.password");
            DRIVER = properties.getProperty("jdbc.driver");
            URL = properties.getProperty("jdbc.url");
        } catch (IOException e) {
            throw new RuntimeException("读取配置文件异常", e);
        }
    }

    /**
     * 获取数据库连接
     *
     * @return 数据库连接
     */
    public Connection getConnection() {
        try {
            Class.forName(DRIVER);//注册驱动
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);//获取连接
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("找不到数据库驱动", e);
        } catch (SQLException e) {
            throw new RuntimeException("获取数据库连接失败", e);
        }
        return connection;
    }

    /**
     * 执行更新操作
     *
     * @param sql    SQL语句
     * @param params 执行参数
     * @return 执行结果
     * @throws SQLException
     */
    public boolean updateByPreparedStatement(String sql, List<?> params) throws SQLException {
        boolean flag = false;

        int result = -1;//表示当前用户执行添加删除或者修改的时候所印象的数据库的行数
        pstmt = connection.prepareStatement(sql);
        int index = 1;
        // 填充sql语句中的占位符
        if (params != null && !params.isEmpty()) {
            for (int i = 0; i < params.size(); i++) {
                pstmt.setObject(index++, params.get(i));
            }
        }
        result = pstmt.executeUpdate();
        flag = result > 0 ? true : false;
        return flag;
    }

    /**
     * 执行查询操作
     *
     * @param sql    SQL语句
     * @param params 执行参数
     * @return 查询结果
     * @throws SQLException
     */
    public List<Map<String, Object>> findResult(String sql, List<?> params)
            throws SQLException {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        int index = 1;
        pstmt = connection.prepareStatement(sql);
        if (params != null && !params.isEmpty()) {
            for (int i = 0; i < params.size(); i++) {
                pstmt.setObject(index++, params.get(i));
            }
        }
        resultSet = pstmt.executeQuery();
        ResultSetMetaData metaData = resultSet.getMetaData();
        int cols_len = metaData.getColumnCount();
        while (resultSet.next()) {
            Map<String, Object> map = new HashMap<String, Object>();
            for (int i = 0; i < cols_len; i++) {
                String cols_name = metaData.getColumnName(i + 1);
                Object cols_value = resultSet.getObject(cols_name);
                if (cols_value == null) {
                    cols_value = "";
                }
                map.put(cols_name, cols_value);
            }
            list.add(map);
        }
        return list;
    }

    /**
     * 释放资源
     */
    public void releaseConn() {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
