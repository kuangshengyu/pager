package com.demo.pager.dao.impl;

import com.demo.pager.basic.Constant;
import com.demo.pager.dao.StudentDao;
import com.demo.pager.entity.Pager;
import com.demo.pager.entity.Student;
import com.demo.pager.util.JdbcUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by kuang on 12/13 18:07
 */
public class JdbcSqlStudentDaoImpl implements StudentDao {
    @Override
    public Pager<Student> findStudent(Student searchModel, int pageNum, int pageSize) {
        Pager<Student> result = null;
        List<Object> paramsList = new ArrayList<>();
        String stuName = searchModel.getStuName();
        int gender = searchModel.getGender();

        StringBuilder sql = new StringBuilder("select * from t_student where 1=1");
        StringBuilder countSql = new StringBuilder("select count(*) as totalRecord from t_student where 1=1");
        if (stuName != null && !stuName.equals("")) {
            sql.append(" and stu_name like ?");
            countSql.append(" and stu_name like ?");
            paramsList.add("%" + stuName + "%");
        }
        if (gender == Constant.GENDER_MALE || gender == Constant.GENDER_FEMALE) {
            sql.append(" and gender = ?");
            countSql.append(" and gender = ?");
            paramsList.add(gender);
        }

        //起始索引
        int fromIndex = pageSize * (pageNum - 1);

        //使用limit关键字进行分页
        sql.append(" limit "+fromIndex+", "+pageSize);

        List<Student> studentList=new ArrayList<>();
        JdbcUtil jdbcUtil = new JdbcUtil();
        jdbcUtil.getConnection();
        try {
            List<Map<String, Object>> countResult = jdbcUtil.findResult(countSql.toString(), paramsList);
            Map<String,Object> countMap=countResult.get(0);
            int totalRecord= ((Number)countMap.get("totalRecord")).intValue();
            List<Map<String, Object>> mapList = jdbcUtil.findResult(sql.toString(), paramsList);

            if (mapList != null) {
                for (Map<String, Object> map : mapList) {
                    Student student = new Student(map);
                    studentList.add(student);
                }
            }

            //获取总页数
            int totalPage = totalRecord % pageSize != 0 ? totalRecord / pageSize + 1 : totalRecord / pageSize;
            result=new Pager<>(pageSize,pageNum,totalRecord,totalPage,studentList);
        } catch (SQLException e) {
            throw new RuntimeException("查询所有数据异常", e);
        } finally {
            jdbcUtil.releaseConn();
        }
        return result;
    }
}
