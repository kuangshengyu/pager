package com.demo.pager.dao.impl;

import com.demo.pager.basic.Constant;
import com.demo.pager.dao.StudentDao;
import com.demo.pager.entity.Pager;
import com.demo.pager.entity.Student;
import com.demo.pager.util.JdbcUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by kuang on 12/12 16:51
 */
public class SublistStudentDaoImpl implements StudentDao {

    @Override
    public Pager<Student> findStudent(Student searchModel, int pageNum, int pageSize) {
        List<Student> allStudent = this.getAllStudent(searchModel);
        Pager<Student> pager = new Pager<Student>(pageNum, pageSize, allStudent);
        return pager;
    }

    public List<Student> getAllStudent(Student searchModel) {
        List<Student> result = new ArrayList<>();
        List<Object> paramsList = new ArrayList<>();
        String stuName = searchModel.getStuName();
        int gender = searchModel.getGender();

        StringBuilder sql = new StringBuilder("select * from t_student where 1=1");
        if (stuName != null && !stuName.equals("")) {
            sql.append(" and stu_name like ?");
            paramsList.add("%" + stuName + "%");
        }
        if (gender == Constant.GENDER_MALE || gender == Constant.GENDER_FEMALE) {
            sql.append(" and gender = ?");
            paramsList.add(gender);
        }

        JdbcUtil jdbcUtil = new JdbcUtil();
        jdbcUtil.getConnection();
        try {
            List<Map<String, Object>> mapList = jdbcUtil.findResult(sql.toString(), paramsList);

            if (mapList != null) {
                for (Map<String, Object> map : mapList) {
                    Student student = new Student(map);
                    result.add(student);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("查询所有数据异常", e);
        } finally {
            jdbcUtil.releaseConn();
        }
        return result;
    }
}
