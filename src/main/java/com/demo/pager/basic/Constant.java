package com.demo.pager.basic;

/**
 * Created by kuang on 12/12 17:09
 */
public class Constant {
    /**
     * 男
     */
    public static final int GENDER_MALE = 1;
    /**
     * 女
     */
    public static final int GENDER_FEMALE = 2;

    /**
     * 默认每页数据条数
     */
    public static final int DEFAULT_PAGE_SIZE = 10;

    /**
     * 默认显示第几页
     */
    public static final int DEFAULT_PAGE_NUM = 1;

    /**
     * 默认性别
     */
    public static final int DEFAULT_GENDER = 0;
}
