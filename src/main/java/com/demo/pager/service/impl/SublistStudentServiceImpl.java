package com.demo.pager.service.impl;

import com.demo.pager.dao.StudentDao;
import com.demo.pager.dao.impl.SublistStudentDaoImpl;
import com.demo.pager.entity.Pager;
import com.demo.pager.entity.Student;
import com.demo.pager.service.StudentService;

/**
 * Created by kuang on 12/12 19:46
 */
public class SublistStudentServiceImpl implements StudentService {

    private StudentDao studentDao;



    public SublistStudentServiceImpl() {
        this.studentDao = new SublistStudentDaoImpl();
    }

    @Override
    public Pager<Student> findStudent(Student searchModel, int pageNum, int pageSize) {
        return studentDao.findStudent(searchModel,pageNum,pageSize);
    }

    public StudentDao getStudentDao() {
        return studentDao;
    }

    public void setStudentDao(StudentDao studentDao) {
        this.studentDao = studentDao;
    }
}
