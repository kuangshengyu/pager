package com.demo.pager.service;

import com.demo.pager.entity.Pager;
import com.demo.pager.entity.Student;

/**
 * Created by kuang on 12/12 16:48
 */
public interface StudentService {

    /**
     * 根据查询条件，查询学生分页信息
     *
     * @param searchModel 封装查询条件
     * @param pageNum     查询第几页数据
     * @param pageSize    每页显示多少条数据
     * @return 查询结果
     */
    public Pager<Student> findStudent(Student searchModel, int pageNum, int pageSize);
}
