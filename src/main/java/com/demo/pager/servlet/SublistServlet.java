package com.demo.pager.servlet;

import com.demo.pager.basic.Constant;
import com.demo.pager.entity.Pager;
import com.demo.pager.entity.Student;
import com.demo.pager.service.StudentService;
import com.demo.pager.service.impl.SublistStudentServiceImpl;
import com.demo.pager.util.StringUtil;

import java.io.IOException;

/**
 * Created by kuang on 12/12 19:49
 */
public class SublistServlet extends javax.servlet.http.HttpServlet {

    private StudentService studentService = new SublistStudentServiceImpl();


    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        //接收request里的参数
        String stuName = request.getParameter("stuName");//学生姓名

        int gender = Constant.DEFAULT_GENDER;
        String genderStr = request.getParameter("gender");//性别
        if (genderStr != null && !"".equals(genderStr)) {
            gender = Integer.parseInt(genderStr);
        }

        // 校验pageNum参数输入合法性
        String pageNumStr = request.getParameter("pageNum");
        if (pageNumStr != null && !StringUtil.isNum(pageNumStr)) {
            request.setAttribute("errorMsg", "参数传输错误");
            request.getRequestDispatcher("sublistStudent.jsp").forward(request, response);
            return;
        }

        int pageNum = Constant.DEFAULT_PAGE_NUM; //显示第几页数据
        if (pageNumStr != null && !"".equals(pageNumStr.trim())) {
            pageNum = Integer.parseInt(pageNumStr);
        }

        int pageSize = Constant.DEFAULT_PAGE_SIZE;  // 每页显示多少条记录
        String pageSizeStr = request.getParameter("pageSize");
        if (pageSizeStr != null && !"".equals(pageSizeStr.trim())) {
            pageSize = Integer.parseInt(pageSizeStr);
        }

        //组装查询条件
        Student searchModel = new Student();
        searchModel.setStuName(stuName);
        searchModel.setGender(gender);

        //调用Service获取查询结果

        Pager<Student> result = studentService.findStudent(searchModel, pageNum, pageSize);

        // 返回结果到页面
        request.setAttribute("result", result);
        request.setAttribute("stuName", stuName);
        request.setAttribute("gender", gender);

        request.getRequestDispatcher("sublistStudent.jsp").forward(request, response);
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        this.doPost(request, response);
    }
}
